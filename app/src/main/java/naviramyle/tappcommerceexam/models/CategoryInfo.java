package naviramyle.tappcommerceexam.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Ramyle on 09/06/2016.
 */
public class CategoryInfo implements Serializable{

    @SerializedName("category-key")
    String key;

    @SerializedName("category-localized-title")
    String title;

    @SerializedName("items")
    List<Item> items;

    public String getKey() {
        return key;
    }

    public String getTitle() {
        return title;
    }

    public List<Item> getItems() {
        return items;
    }

    public class Item implements Serializable {
        @SerializedName("type")
        String type;

        @SerializedName("category")
        Category category;

        @SerializedName("product")
        Product product;

        public String getType() {
            return type;
        }

        public Category getCategory() {
            return category;
        }

        public Product getProduct() {
            return product;
        }
    }

}
