package naviramyle.tappcommerceexam.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ramyle on 08/06/2016.
 */
public class Category implements Serializable {
    @SerializedName("product-category-id")
    long id;

    @SerializedName("product-category-image-id")
    long imageId;

    @SerializedName("product-category-rank")
    int rank;

    @SerializedName("category-key")
    String key;

    @SerializedName("category-localized-title")
    String title;

    @SerializedName("type")
    String type;


    public long getId() {
        return id;
    }

    public long getImageId() {
        return imageId;
    }

    public int getRank() {
        return rank;
    }

    public String getKey() {
        return key;
    }

    public String getTitle() {
        return title;
    }

    public String getType() {
        return type;
    }
}
