package naviramyle.tappcommerceexam.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ramyle on 09/06/2016.
 */
public class Product implements Serializable {
    @SerializedName("engine-product-id")
    String enginePid;

    @SerializedName("localization-key")
    String locKey;

    @SerializedName("type")
    String type;

    @SerializedName("localized-description")
    String locDescription;

    @SerializedName("category-key")
    String catKey;

    @SerializedName("denomination-currency")
    String demCurrency;

    @SerializedName("currency")
    String currency;

    @SerializedName("product-promotion-message")
    String message;

    @SerializedName("denomination-amount")
    long demAmount;

    @SerializedName("parent-category-id")
    long parCatId;

    @SerializedName("form")
    String form;

    @SerializedName("merchant")
    String merchant;

    @SerializedName("price")
    long price;

    @SerializedName("product-id")
    long pid;

    @SerializedName("product-image-id")
    long imageId;

    @SerializedName("srp")
    long srp;

    public String getEnginePid() {
        return enginePid;
    }

    public String getLocKey() {
        return locKey;
    }

    public String getType() {
        return type;
    }

    public String getLocDescription() {
        return locDescription;
    }

    public String getCatKey() {
        return catKey;
    }

    public String getDemCurrency() {
        return demCurrency;
    }

    public String getCurrency() {
        return currency;
    }

    public String getMessage() {
        return message;
    }

    public long getDemAmount() {
        return demAmount;
    }

    public long getParCatId() {
        return parCatId;
    }

    public String getForm() {
        return form;
    }

    public String getMerchant() {
        return merchant;
    }

    public long getPrice() {
        return price;
    }

    public long getPid() {
        return pid;
    }

    public long getImageId() {
        return imageId;
    }

    public long getSrp() {
        return srp;
    }
}
