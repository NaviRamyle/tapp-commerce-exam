package naviramyle.tappcommerceexam.interfaces;

import naviramyle.tappcommerceexam.models.CategoryInfo;

/**
 * Created by Ramyle on 09/06/2016.
 */
public interface OnItemClickListener {
    void onItemClick(String type, String title, long id, CategoryInfo.Item item);
}
