package naviramyle.tappcommerceexam.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import naviramyle.tappcommerceexam.Constants;
import naviramyle.tappcommerceexam.R;
import naviramyle.tappcommerceexam.models.Category;

/**
 * Created by Ramyle on 09/06/2016.
 */
public class CategoriesAdapter extends BaseAdapter {

    Context context;
    LayoutInflater inflater;
    List<Category> categories;

    public CategoriesAdapter(Context context, List<Category> categories) {
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.categories = categories;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Category getItem(int position) {
        return categories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return categories.get(position).getId();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Category category = categories.get(position);

        CategoryViewHolder holder;

        if (view == null) {
            view = inflater.inflate(R.layout.list_item_category, parent, false);
            holder = new CategoryViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (CategoryViewHolder) view.getTag();
        }

        Picasso.with(context).load(Constants.CATEGORY_ICON + category.getImageId()).into(holder.iv_icon);
        holder.tv_title.setText(category.getTitle());

        return view;
    }

    public static class CategoryViewHolder {
        @Bind(R.id.iv_icon)
        ImageView iv_icon;

        @Bind(R.id.tv_title)
        TextView tv_title;

        public CategoryViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
