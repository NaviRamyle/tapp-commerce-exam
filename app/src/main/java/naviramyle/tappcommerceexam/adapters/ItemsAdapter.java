package naviramyle.tappcommerceexam.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import naviramyle.tappcommerceexam.Constants;
import naviramyle.tappcommerceexam.R;
import naviramyle.tappcommerceexam.interfaces.OnItemClickListener;
import naviramyle.tappcommerceexam.models.Category;
import naviramyle.tappcommerceexam.models.CategoryInfo;
import naviramyle.tappcommerceexam.models.Product;

/**
 * Created by Ramyle on 09/06/2016.
 */
public class ItemsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<CategoryInfo.Item> items;
    OnItemClickListener onItemClickListener;

    public ItemsAdapter(Context context, OnItemClickListener onItemClickListener, List<CategoryInfo.Item> items) {
        this.items = items;
        this.onItemClickListener = onItemClickListener;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                return new CategoryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_category, parent, false));
            case 2:
                return new ProductViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_product, parent, false));
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder h, int position) {

        final CategoryInfo.Item item = items.get(position);
        if (item.getType().equals(Constants.KEY_CATEGORY)) {
            CategoryViewHolder holder = (CategoryViewHolder) h;
            final Category category = item.getCategory();
            Picasso.with(context).load(Constants.CATEGORY_ICON + category.getImageId()).into(holder.iv_icon);
            holder.tv_title.setText(category.getTitle());
            holder.ll_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(Constants.KEY_CATEGORY, category.getTitle(), category.getId(), item);
                }
            });
        } else if (item.getType().equals(Constants.KEY_PRODUCT)) {
            ProductViewHolder holder = (ProductViewHolder) h;
            final Product product = item.getProduct();
            Picasso.with(context).load(Constants.CATEGORY_ICON + product.getImageId()).into(holder.iv_icon);
            holder.tv_title.setText(product.getLocDescription());
            holder.tv_price.setText(product.getCurrency() + " " + product.getPrice());
            holder.ll_container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onItemClickListener.onItemClick(Constants.KEY_PRODUCT, "", product.getPid(), item);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        CategoryInfo.Item item = items.get(position);
        if (item.getType().equals(Constants.KEY_CATEGORY)) {
            return 1;
        } else if (item.getType().equals(Constants.KEY_PRODUCT)) {
            return 2;
        } else {
            return 1;
        }
    }

    public static class CategoryViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ll_container)
        LinearLayout ll_container;

        @Bind(R.id.iv_icon)
        ImageView iv_icon;

        @Bind(R.id.tv_title)
        TextView tv_title;

        public CategoryViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public static class ProductViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.ll_container)
        LinearLayout ll_container;

        @Bind(R.id.iv_icon)
        ImageView iv_icon;

        @Bind(R.id.tv_title)
        TextView tv_title;

        @Bind(R.id.tv_price)
        TextView tv_price;

        public ProductViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
