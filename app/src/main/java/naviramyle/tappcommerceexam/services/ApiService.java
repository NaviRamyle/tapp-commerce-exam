package naviramyle.tappcommerceexam.services;

import java.util.List;

import naviramyle.tappcommerceexam.Constants;
import naviramyle.tappcommerceexam.models.Category;
import naviramyle.tappcommerceexam.models.CategoryInfo;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by agtechlabsivan on 8/24/15.
 */
public interface ApiService {

    @GET(Constants.API_PRODUCT_CATEGORIES)
    Call<List<Category>> getCategories();

    @GET(Constants.API_PRODUCT_CATEGORY)
    Call<CategoryInfo> getCategoryInfo(@Path("category_id") long category_id);
}
