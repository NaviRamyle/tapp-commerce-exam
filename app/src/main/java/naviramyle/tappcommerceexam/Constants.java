package naviramyle.tappcommerceexam;

/**
 * Created by Ramyle on 08/06/2016.
 */
public class Constants {


    public static final String BASE_API_URL = "https://sandbox-market.tapp.fi/";

    public static final String API_VERSION = "api/1/";

    public static final String API_PRODUCT_CATEGORIES = API_VERSION + "product-categories/";
    public static final String API_PRODUCT_CATEGORY = API_VERSION + "product-categories/{category_id}";

    public static final String CATEGORY_ICON = BASE_API_URL + "app/product-image/";

    public static final String KEY_CATEGORY_ID = "category_id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_CATEGORY = "category";
    public static final String KEY_PRODUCT = "product";
    public static final String KEY_ITEM = "item";
}
