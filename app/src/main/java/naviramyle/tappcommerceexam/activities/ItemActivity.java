package naviramyle.tappcommerceexam.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import naviramyle.tappcommerceexam.Constants;
import naviramyle.tappcommerceexam.R;
import naviramyle.tappcommerceexam.adapters.ItemsAdapter;
import naviramyle.tappcommerceexam.interfaces.OnItemClickListener;
import naviramyle.tappcommerceexam.models.CategoryInfo;
import naviramyle.tappcommerceexam.services.ApiService;
import naviramyle.tappcommerceexam.services.ApiServiceAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Ramyle on 09/06/2016.
 */
public class ItemActivity extends BaseAppCompatActivity {

    @Bind(R.id.tv_empty)
    TextView tv_empty;

    @Bind(R.id.rv_items)
    RecyclerView rv_items;

    ItemsAdapter itemsAdapter;

    String title;
    long category_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        title = getIntent().getStringExtra(Constants.KEY_TITLE);
        category_id = getIntent().getLongExtra(Constants.KEY_CATEGORY_ID, -9999);

        setContentView(R.layout.activity_item);
        ApiService service = ApiServiceAdapter.getInstance();

        Call<CategoryInfo> call = service.getCategoryInfo(category_id);
        call.enqueue(new Callback<CategoryInfo>() {
            @Override
            public void onResponse(Call<CategoryInfo> call, Response<CategoryInfo> response) {
                setList(response.body().getItems());
            }

            @Override
            public void onFailure(Call<CategoryInfo> call, Throwable t) {
                t.printStackTrace();
            }
        });

        setActionBar();
    }

    private void setList(List<CategoryInfo.Item> items) {
        OnItemClickListener onItemClickListener = new OnItemClickListener() {
            @Override
            public void onItemClick(String type, String title, long id, CategoryInfo.Item item) {
                if (type.equals(Constants.KEY_CATEGORY)) {
                    Intent intent = new Intent(getActivity(), ItemActivity.class);
                    intent.putExtra(Constants.KEY_TITLE, title);
                    intent.putExtra(Constants.KEY_CATEGORY_ID, id);
                    startActivity(intent);
                } else if (type.equals(Constants.KEY_PRODUCT)) {
                    Intent intent = new Intent(getActivity(), ProductDetailsActivity.class);
                    intent.putExtra(Constants.KEY_ITEM, item);
                    startActivity(intent);
                }
            }
        };

        itemsAdapter = new ItemsAdapter(this, onItemClickListener, items);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        rv_items.setLayoutManager(llm);
        rv_items.setAdapter(itemsAdapter);

        if (items.size() == 0) {
            tv_empty.setVisibility(View.VISIBLE);
        }
    }

    private void setActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(title);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
