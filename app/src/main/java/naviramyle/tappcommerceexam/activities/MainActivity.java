package naviramyle.tappcommerceexam.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import butterknife.Bind;
import naviramyle.tappcommerceexam.Constants;
import naviramyle.tappcommerceexam.R;
import naviramyle.tappcommerceexam.adapters.CategoriesAdapter;
import naviramyle.tappcommerceexam.models.Category;
import naviramyle.tappcommerceexam.services.ApiService;
import naviramyle.tappcommerceexam.services.ApiServiceAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseAppCompatActivity {
    @Bind(R.id.lv_categories)
    ListView lv_categories;

    CategoriesAdapter adapterAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ApiService service = ApiServiceAdapter.getInstance();

        Call<List<Category>> call = service.getCategories();
        call.enqueue(new Callback<List<Category>>() {
            @Override
            public void onResponse(Call<List<Category>> call, Response<List<Category>> response) {
                setList(response.body());
            }

            @Override
            public void onFailure(Call<List<Category>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void setList(final List<Category> categories) {
        adapterAdapter = new CategoriesAdapter(getActivity(), categories);
        Collections.sort(categories, new Comparator<Category>() {
            @Override
            public int compare(Category lhs, Category rhs) {
                return rhs.getRank() - lhs.getRank();
            }
        });
        lv_categories.setAdapter(adapterAdapter);
        lv_categories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ItemActivity.class);
                intent.putExtra(Constants.KEY_TITLE, categories.get(position).getTitle());
                intent.putExtra(Constants.KEY_CATEGORY_ID, id);
                startActivity(intent);
            }
        });
    }
}
