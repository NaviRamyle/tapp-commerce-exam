package naviramyle.tappcommerceexam.activities;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import butterknife.Bind;
import naviramyle.tappcommerceexam.Constants;
import naviramyle.tappcommerceexam.R;
import naviramyle.tappcommerceexam.models.CategoryInfo;
import naviramyle.tappcommerceexam.models.Product;

/**
 * Created by Ramyle on 09/06/2016.
 */
public class ProductDetailsActivity extends BaseAppCompatActivity {

    @Bind(R.id.iv_icon)
    ImageView iv_icon;

    @Bind(R.id.tv_title)
    TextView tv_title;

    @Bind(R.id.tv_price)
    TextView tv_price;

    @Bind(R.id.tv_details)
    TextView tv_details;

    CategoryInfo.Item item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        item = (CategoryInfo.Item) getIntent().getSerializableExtra(Constants.KEY_ITEM);

        setContentView(R.layout.activity_product_details);

        setDetails();

        setActionBar();
    }

    private void setDetails() {
        Product product = item.getProduct();


        Picasso.with(this).load(Constants.CATEGORY_ICON + product.getImageId()).into(iv_icon);
        tv_title.setText(product.getLocDescription());
        tv_price.setText(product.getCurrency() + " " + product.getPrice());
        tv_details.setText(new Gson().toJson(product).replace("{", "").replace("}", "").replace(",", "\n").replace("\"", "").replace(":", " = "));
    }

    private void setActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
