package naviramyle.tappcommerceexam.activities;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import butterknife.ButterKnife;

/**
 * Created by Ramyle on 08/06/2016.
 */
public class BaseAppCompatActivity extends AppCompatActivity {
    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    public Activity getActivity() {
        return this;
    }
}
